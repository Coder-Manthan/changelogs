Changes shipped to be shipped in release 210507:

Edition changes:
All editions:
    - Added FireDragon as default browser - our own customized Librewolf fork with enhanced KDE integration & saner defaults, making the browser easier to use without having to customize all the settings (expect XFCE)
    - A notification which remembers to restore the snapshot when booted into one has been added
    - Added Optimus manager (priority to highest) and made hybrid mode as default for NVIDIA users
    - Disabled preload in live
    - Disabled Plymouth in live to make spotting boot issues easier
    - Garuda applications are now fixed releases
    - Fixed polkit rules used for opening applications / mounting without sudo
    - New bootanimations have been added
    - Requirement checks are now in place to prevent installation on machines which would not run Garuda properly - 3.5GB of RAM & 30GB storage space are needed to actually start the installer now 
Dr460nized:
    - Added candy-icons to make Sweet global theme work
    - Added thunderbird-appmenu as it is updated to the current thunderbird version finally
    - Added uncluttered neofetch config
    - Back to magic lamp effect (minimize)https://aur.archlinux.org/packages/kvantum-theme-sweet-git/
    - Disabled translucency & left side trigger which was causing a lot of confusion
    - Enabled confirmation dialog for the logout plasmoid
    - Fixed bad opacity values of latte-dock
    - Fixed blurry start menu buttom
    - Updated Latte layout
    - Updated Sweet theme (includes different Plasma loading screen)
    - Switched to Pipewire
GNOME:
    - Fix Alacritty kerning issue caused by typo
    - Removed all extensions due to GNOME 40 update. This marked the end of our heavily customized GNOME version as supporting it without major changes like these is not possible. (https://forum.garudalinux.org/t/gnome-extensions-removed-due-to-being-broken-by-release-40-yet-again/6735) 
    - Switched to Pipewire
i3:
    - Added azote for easily changing the background
LXQt-Kwin:
    - Switched to BeautyLine icons
    - Switched to Pipewire
Qtile:
    - Added minimize, maximize function using tabs.
    - Changed colors to bar and windows borders.
    - Changed to picom-ibhagwan-git for blur!
    - Replaced WindowName widget with Tasklist for windows tabs.
    - Replaced battery widget with network widget.
    - Updated qtile config as per latest release.
    - Switched to Pipewire!
Sway:
    - New edition in store by @OdiousImp
Wayfire:
    - Added greetd-qtgreet as displaymanager
    - Changed to Sweet theme
    - Changed to Thunar as filemanager & gammastep instead of redshift
    - Updated keybindings
Dr460nized-dev:
    - For people who want to help testing some new stuff, this is basically dr460nized with experimental extras (*possibly contains bugs*), please provide feedback in the forum.
    - KwinFT instead of Kwin (New fork focusing on improving codebase & delivering improvements over Kwin - https://www.subdiff.org/blog/2020/the-k-win-ft-project/)
    - Linux-cacule as kernel, focused on providing a very smooth user experience with minimal input lag (https://forum.garudalinux.org/t/linux-cacule-feedback-testing/7672/39)
    - Zram-generator instead of systemd-swap (recommended by its dev, utilised by systemd)
    
Application & settings changes:
BeautyLine icons:
    - Added more icons
    - Fixed wrong symlinks & empty icons
Garuda-assistant:
    - Added option to refresh keyrings to fix signature errors
    - Added option to switch shells 
Garuda-downloader:
    - New application made by @TNE which downloads latest isos, is able to use existing isos as source for downloads - downloading only the different parts of the iso files saving you possibly a lot of bandwidgh. its available on Windows & Linux
Garuda-fish-config:
    - "helpme" alias to cht.sh, an interactive cheatsheet for a lot of things
    - Ls aliases show icons 
    - Removed the fish-systemd function
    - Removed the paru --bottomup alias as it was causing issues when run alone, shipped in /etc/paru.conf now
    - Updated done function
    - Using neofetch-git instead of paleofetch-git
Garuda-hooks:
    - Fix btrfs openswap
Garuda-network-assistant:
    - Check available commands before trying
    - Updated scripts
Garuda-setup-assistant:
    - Added FireDragon to browsers
    - Fixed some strings
    - Removed dotfile updating on system update as this was causing issues
Garuda-starship-config:
    - Fixed the issues caused by a change of the config syntax
Garuda-zsh-config:
    - Enabled tab selection
    - Using neofetch-git instead of paleofetch-git
    
Server & infrastructure changes:
Garuda-tools:
    - Added zsync2 support for downloads
GitLab:
    - All settings packages have their fixed release PKGBUILD included in the respective repo now to decrease time spent updating repos
    - Settings packages now follow semantic versioning for a more stable experience, starting at version 1.0.0
Nextcloud:
    - Updated to version 21
Website:
    - Added package list to download options
